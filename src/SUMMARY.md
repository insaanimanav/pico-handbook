# Summary

- [Intro to the Pico](pico.md)
- [Introduction to the Toolchain](toolchain.md)
  - [Install everything](toolchain/install.md)
  - [Intro to Make and Cmake](toolchain/make-cmake.md)
