# Installation

## Install the arm toolchain

```shell
sudo apt update
sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential 
```

## Get yourself the pico-sdk

SO the good folks at the raspi-foundation have kind of compiled an sdk for the users to easily compile and build programms for the pico 
So to get the SDK you have to clone the repo

```shell
 git clone -b master https://github.com/raspberrypi/pico-sdk.git
 cd pico-sdk
 git submodule update --init
 cd ..
 git clone -b master https://github.com/raspberrypi/pico-examples.git
 ```

## Get the examples

 Now let us clone the examples that we are going to use for the rest of this book 

```shell
 https://github.com/raspberrypi/pico-examples
 ```

 That's it for the setup
